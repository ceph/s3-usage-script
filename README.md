# Ceph S3 usage

A simple script to check your total S3 usage. Works only on [Ceph S3 implementation](https://docs.ceph.com/en/latest/radosgw/s3/serviceops/#get-usage-stats).
It also provides an exporter daemon mode that allows exposing Prometheus-compatible metrics.

## Installation & authentication

Several methods are available for installing the usage script / exporter.
All installation methods rely on the availability of S3 access credentials,
in the same way is supported by the `aws` cli (usually `~/.aws/credentials`, but also via other environment variables as documented in <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>).

### Binary

Binary downloads for x86 and ARM platforms are provided as part of [Gitlab releases](https://gitlab.cern.ch/ceph/s3-usage-script/-/releases).

The binary takes the following optional parameters:
```
./cephs3usage -h
Usage of ./cephs3usage:
  -debug
    	Enable debug logging
  -endpoint string
    	S3 endpoint to make query against (default "https://s3.cern.ch")
  -json
    	Use JSON output instead of XML.
  -region string
    	S3 region (default "us-east-1")
  -exporter string
        Run a Prometheus-compatible exporter on the specified address
```

Example XML output:

```xml
<Usage>
    <Entries></Entries>
    <Summary>
        <QuotaMaxBytes>536870912000</QuotaMaxBytes>
        <QuotaMaxBuckets>100</QuotaMaxBuckets>
        <QuotaMaxObjCount>-1</QuotaMaxObjCount>
        <QuotaMaxBytesPerBucket>-1</QuotaMaxBytesPerBucket>
        <QuotaMaxObjCountPerBucket>-1</QuotaMaxObjCountPerBucket>
        <TotalBytes>117482818763</TotalBytes>
        <TotalBytesRounded>117627129856</TotalBytesRounded>
        <TotalEntries>46726</TotalEntries>
    </Summary>
    <CapacityUsed>
        <User>
            <Buckets>
                <Entry>
                    <Bucket>example-bucket</Bucket>
                    <Bytes>2207892190</Bytes>
                    <Bytes_Rounded>2209542144</Bytes_Rounded>
                </Entry>
            </Buckets>
        </User>
    </CapacityUsed>
</Usage>
```

### Container

A container image is provided as well: <https://gitlab.cern.ch/ceph/s3-usage-script/container_registry>

The image is multi-arch and can be run on x86 and ARM platforms.

Example usage:

```sh
podman run -e AWS_ACCESS_KEY_ID=xxx -e AWS_SECRET_ACCESS_KEY=xxx \
    gitlab-registry.cern.ch/ceph/s3-usage-script:latest -- \
    -endpoint https://s3.example.com -region us-east-1
```

### Helm chart

The Helm chart in the `chart/` directory enables quick and simple deployments in Kubernetes environments.
All Helm values are documented in [chart/values.yaml](chart/values.yaml).
A minimal deployment may look like this:

```yaml
config:
  endpoint: "https://s3.example.com"
  region: "us-east-1"
  env:
    AWS_ACCESS_KEY_ID: "xxx"
    AWS_SECRET_ACCESS_KEY: "xxx"
```

Then simply run `helm install "s3-exporter" -f my-values.yaml chart/` to install the component in your Kubernetes cluster.

*Note:* the credentials can also be provided by mounting `.aws/config` and `.aws/credentials` files, see the Helm values `volumes` and `volumeMounts`.

## Exporter

When run with the `-exporter=127.0.0.1:8080` flag, an HTTP server will expose Prometheus-compatible metrics that at <http://localhost:8080/metrics>.

Example metrics:

```
# HELP ceph_s3_bucket_bytes Storage space used by a single bucket, in bytes.
# TYPE ceph_s3_bucket_bytes gauge
ceph_s3_bucket_bytes{bucket="example-bucket",s3_endpoint="https://s3.cern.ch",s3_region="us-east-1"} 2.20789219e+09

# HELP ceph_s3_quota_max_buckets Total number of buckets allowed for this account.
# TYPE ceph_s3_quota_max_buckets gauge
ceph_s3_quota_max_buckets{s3_endpoint="https://s3.cern.ch",s3_region="us-east-1"} 100

# HELP ceph_s3_quota_max_bytes Total storage space quota for this account, in bytes.
# TYPE ceph_s3_quota_max_bytes gauge
ceph_s3_quota_max_bytes{s3_endpoint="https://s3.cern.ch",s3_region="us-east-1"} 5.36870912e+11

# HELP ceph_s3_total_bytes Total storage space used by all buckets for this account, in bytes.
# TYPE ceph_s3_total_bytes gauge
ceph_s3_total_bytes{s3_endpoint="https://s3.cern.ch",s3_region="us-east-1"} 1.17482818763e+11

# HELP ceph_s3_up Indidicates if the exporter is healthy
# TYPE ceph_s3_up gauge
ceph_s3_up{s3_endpoint="https://s3.cern.ch",s3_region="us-east-1"} 1
```

## License

The source code is licensed under the [Apache 2.0 license](./LICENSE).
