package main

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	v4 "github.com/aws/aws-sdk-go/aws/signer/v4"
)

var endpoint = flag.String("endpoint", "https://s3.cern.ch", "S3 endpoint to make query against")
var region = flag.String("region", "us-east-1", "S3 region")
var debug = flag.Bool("debug", false, "Enable debug logging")
var jsonOutput = flag.Bool("json", false, "Use JSON output (default XML)")
var exporterAddr = flag.String("exporter", "", "Run a Prometheus exporter on the specified address, e.g. '127.0.0.1:8080'")

func logRequest(req *http.Request) {
	fmt.Println("Request:")
	fmt.Printf("Method: %s\n", req.Method)
	fmt.Printf("URL: %s\n", req.URL.String())
	fmt.Println("Headers:")
	for key, values := range req.Header {
		for _, value := range values {
			fmt.Printf("%s: %s\n", key, value)
		}
	}
}

func logResponse(resp *http.Response) {
	fmt.Println("Response:")
	fmt.Printf("Status: %s\n", resp.Status)
	fmt.Println("Headers:")
	for key, values := range resp.Header {
		for _, value := range values {
			fmt.Printf("%s: %s\n", key, value)
		}
	}
}

func queryUsageEndpoint(endpoint string, region string) (UsageResponse, error) {
	var usage UsageResponse
	session, err := session.NewSession(&aws.Config{
		Endpoint: aws.String(endpoint),
	})
	if err != nil {
		return usage, fmt.Errorf("Failed to create AWS session: %w", err)
	}

	endpointURL, err := url.Parse(endpoint)
	if err != nil {
		return usage, fmt.Errorf("Invalid endpoint URL: %w", err)
	}

	q := endpointURL.Query()
	q.Set("usage", "True")
	endpointURL.RawQuery = q.Encode()

	req, err := http.NewRequest("GET", endpointURL.String(), nil)
	if err != nil {
		return usage, err
	}

	signer := v4.NewSigner(session.Config.Credentials)
	_, err = signer.Sign(req, nil, "s3", region, time.Now())
	if err != nil {
		return usage, fmt.Errorf("Failed to sign request %s", err)
	}

	if *debug {
		logRequest(req)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Failed to make request: %s", err)
	}

	defer resp.Body.Close()

	if *debug {
		logResponse(resp)
	}

	if resp.StatusCode != http.StatusOK {
		return usage, fmt.Errorf("Got unexpected status code %d", resp.StatusCode)
	}

	// Read response body and parse it as XML
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return usage, fmt.Errorf("Failed to read response body: %w", err)
	}

	if err := xml.Unmarshal(body, &usage); err != nil {
		return usage, fmt.Errorf("Unable to decode XML response body: %w", err)
	}

	return usage, nil
}

func main() {
	flag.Parse()

	if *endpoint == "" {
		log.Fatal("You must specify an S3 endpoint")
	}

	if *exporterAddr != "" {
		log.Fatal(runPrometheusExporter(*exporterAddr))
	}

	usage, err := queryUsageEndpoint(*endpoint, *region)
	if err != nil {
		log.Fatal(err)
	}

	if *jsonOutput {
		printJSON(usage)
	} else {
		printXML(usage)
	}
}

func printXML(obj any) {
	b, err := xml.MarshalIndent(obj, "", "\t") // pretty print
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(b))
}

func printJSON(obj any) {
	b, err := json.MarshalIndent(obj, "", "\t") // pretty print
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(b))
}

type UsageResponse struct {
	XMLName xml.Name `xml:"Usage" json:"-"`
	Text    string   `xml:",chardata" json:"text,omitempty"`
	Entries string   `xml:"Entries", json:"-"`
	Summary struct {
		Text                      string `xml:",chardata" json:"text,omitempty"`
		QuotaMaxBytes             string `xml:"QuotaMaxBytes"`
		QuotaMaxBuckets           string `xml:"QuotaMaxBuckets"`
		QuotaMaxObjCount          string `xml:"QuotaMaxObjCount"`
		QuotaMaxBytesPerBucket    string `xml:"QuotaMaxBytesPerBucket"`
		QuotaMaxObjCountPerBucket string `xml:"QuotaMaxObjCountPerBucket"`
		TotalBytes                string `xml:"TotalBytes"`
		TotalBytesRounded         string `xml:"TotalBytesRounded"`
		TotalEntries              string `xml:"TotalEntries"`
	} `xml:"Summary" json:"Summary,omitempty"`
	CapacityUsed struct {
		Text string `xml:",chardata" json:"text,omitempty"`
		User struct {
			Text    string `xml:",chardata" json:"text,omitempty"`
			Buckets struct {
				Text  string `xml:",chardata" json:"text,omitempty"`
				Entry []struct {
					Text         string `xml:",chardata" json:"text,omitempty"`
					Bucket       string `xml:"Bucket"`
					Bytes        string `xml:"Bytes"`
					BytesRounded string `xml:"Bytes_Rounded"`
				} `xml:"Entry" json:"Entry,omitempty"`
			} `xml:"Buckets" json:"Buckets,omitempty"`
		} `xml:"User" json:"User,omitempty"`
	} `xml:"CapacityUsed" json:"CapacityUsed,omitempty"`
}
