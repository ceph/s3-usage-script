FROM docker.io/library/golang:1.22 as builder

WORKDIR /workspace
# Download dependencies
COPY go.mod go.sum .
RUN go mod download

# Copy source code
COPY *.go .

# Build
ENV CGO_ENABLED=0
RUN go build -o cephs3usage .

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/cephs3usage /bin/
USER nonroot:nonroot

ENTRYPOINT ["/bin/cephs3usage"]
