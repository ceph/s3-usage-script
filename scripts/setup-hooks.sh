#!/bin/sh

# Copy pre-commit hook to the Git hooks directory
cp scripts/pre-commit.sh .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
